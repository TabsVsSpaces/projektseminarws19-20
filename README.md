**Zum Start wird ein laufender MQTT Broker ben�tigt (bspw. mosquitto)**
	
	1.	siehe https://neuendorf-online.de/posts/heimautomatisierung/mqtt-broker-mosquitto-installieren/
	2.	sudo apt-get install mosquitto mosquitto-clients
	
**Libs**

1. sudo apt-get install python3-distutils
2. sudo apt install python3-pip
3. pip install setuptools
4. pip install opcua (alternativ: sudo apt install python-opcua + sudo apt install python-opcua-tools)
5. pip install requests
6. pip install paho-mqtt