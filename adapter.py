    # https://refactoring.guru/design-patterns/adapter
# https://realpython.com/python-dicts/
# translate between service(rest/mqtt) and opcua-methode

import json
from rest import restMethods


from mqtt import mqttMethods


class restAdapter:
    def __init__(self, endpoint, method, auth, username, password):
        self.__endpoint = endpoint
        self.__method = method.lower()
        self.__auth = auth
        self.__username = username
        self.__password = password
        self.__jsonData = {}  #json output
        self.__paramsDict = {} #tmp dict for building the json input
        self.__params = {}  #json input

    def add_param(self, key, value):
        self.__paramsDict.update({key: value})

    def call_endpoint(self):
        self.__params = json.dumps(self.__paramsDict)
        self.__switch_method(self.__method)
        self.__paramsDict.clear()
    
    def get_returnDict(self, key):
        return self.__jsonData.get(key)

    def get_returnObj(self):
        return self.__jsonData

    def __switch_method(self, method):
        switcher = {
            "get": self.__get,
            "put": self.__put,
            "post": self.__post
        }
        send_request = switcher.get(method, "Method not supported!")
        return send_request()

    def __get(self):
        if self.__auth is True:
            self.__jsonData = restMethods.get_request_wthCred(self.__endpoint, self.__method, self.__params,
                                                              self.__username, self.__password)
        else:
            self.__jsonData = restMethods.get_request_noCred(self.__endpoint, self.__method, self.__params)

    def __put(self):
        if self.__auth is True:
            self.__jsonData = restMethods.put_request_wthCred(self.__endpoint, self.__method, self.__params,
                                                              self.__username, self.__password)
        else:
            self.__jsonData = restMethods.put_request_noCred(self.__endpoint, self.__method, self.__params)

    def __post(self):
        if self.__auth is True:
            self.__jsonData = restMethods.post_request_wthCred(self.__endpoint, self.__method, self.__params,
                                                               self.__username, self.__password)
        else:
            self.__jsonData = restMethods.post_request_noCred(self.__endpoint, self.__method, self.__params)

class mqttAdapter:
    def __init__(self, endpoint, method, auth, username, password):
        self.mqtt = mqttMethods()
        self.__hostname = endpoint
        self.__method = method.lower()
        self.__auth = auth
        self.__username = username
        self.__password = password
        self.__data = {}
        self.__paramsDict = {}

    def add_param(self, key, value):
            self.__paramsDict.update({key: value})

    def call_endpoint(self):
        self.__switch_method(self.__method)
        self.__paramsDict.clear()

    def get_returnValue(self, key):
        return self.__data.get(key)

    def getAllReturnValue(self):
        return self.__data

    def __switch_method(self, method):
        switcher = {
            "subscribe": self.__subscribeTopic,
            "publish": self.__publishTopic,
        }
        func = switcher.get(method, "Method not supported!")
        return func()

    def __subscribeTopic(self):
        if self.__auth is True:
            client = self.mqtt.Initialise_clients()
            self.mqtt.AuthConnectBroker(client,self.__username,self.__password,self.__hostname)
            self.__data=self.mqtt.SubscribeTopic(client,self.__paramsDict)
        else:
            client = self.mqtt.Initialise_clients()
            mqttMethods.ConnectToBroker(client,self.__hostname)
            self.__data=self.mqtt.SubscribeTopic(client,self.__paramsDict)

    def __publishTopic(self):

        if self.__auth is True:
            client = self.mqtt.Initialise_clients()
            self.mqtt.AuthConnectBroker(client, self.__username, self.__password, self.__hostname)
            self.__data = self.mqtt.publishTopic(client, self.__paramsDict)
        else:
            client = self.mqtt.Initialise_clients()
            self.mqtt.ConnectToBroker(client, self.__hostname)
            self.__data = self.mqtt.publishTopic(client, self.__paramsDict)



