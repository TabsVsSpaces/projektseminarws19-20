# offers communication methods for the opc-ua server
# contains the logic for the comm. process like rate and necessary variables
# uses the adapter for the mapping details
# https://dresden-elektronik.github.io/deconz-rest-doc/lights/

import threading

from opcua import uamethod, ua

from adapter import restAdapter
from adapter import mqttAdapter
import time


@uamethod
def shift(parent):
    from opcuaServer import opcuaServer
    endpoint = opcuaServer.getValue("ns=2;i=2")
    endpoint = endpoint + "/state"
    rest = restAdapter(endpoint, "put", False, "", "")

    status = opcuaServer.getValue("ns=2;i=18")

    if status is True:
        rest.add_param("on", False)
        status = False
    else:
        rest.add_param("on", True)
        status = True

    try:
        threading.Thread(target=rest.call_endpoint()).start()
        data = rest.get_returnObj()
        data = max(data)
    except Exception as e:
        raise ua.UaError("Error calling endpoint")

    if "success" in data:
        opcuaServer.setValue("ns=2;i=18", status)
    else:
        raise ua.UaError("Request not successful")


@uamethod
def changeColor(parent, color):
    from opcuaServer import opcuaServer

    if opcuaServer.getValue("ns=2;i=18") is True:
        endpoint = opcuaServer.getValue("ns=2;i=2")
        endpoint = endpoint + "/state"
        rest = restAdapter(endpoint, "put", False, "", "")

        color = color.lower()
        if color == "red":
            rest.add_param("hue", 0)

        elif color == "blue":
            rest.add_param("hue", 44670)

        elif color == "green":
            rest.add_param("hue", 21760)

        elif color == "yellow":
            rest.add_param("hue", 10880)
        else:
            raise ua.UaError("Color is unknown !")
    else:
        raise ua.UaError("Not connected")

    try:
        threading.Thread(target=rest.call_endpoint()).start()
        data = rest.get_returnObj()
        data = max(data)
    except Exception as e:
        raise ua.UaError("Error calling endpoint")

    if "success" in data:
        opcuaServer.setValue("ns=2;i=17", data.get("success").get("/lights/1/state/hue"))
    else:
        raise ua.UaError("Request not successful")


@uamethod
def getFullState(parent):
    from opcuaServer import opcuaServer
    endpoint = opcuaServer.getValue("ns=2;i=2")
    rest = restAdapter(endpoint, "get", False, "", "")

    try:
        threading.Thread(target=rest.call_endpoint()).start()
        data = rest.get_returnObj()
    except Exception as e:
        raise ua.UaError("Error calling endpoint")

    opcuaServer.setValue("ns=2;i=3", data.get("etag"))
    opcuaServer.setValue("ns=2;i=4", data.get("manufacturername"))
    opcuaServer.setValue("ns=2;i=5", data.get("modelid"))
    opcuaServer.setValue("ns=2;i=6", data.get("name"))
    opcuaServer.setValue("ns=2;i=7", data.get("swversion"))
    opcuaServer.setValue("ns=2;i=8", data.get("type"))
    opcuaServer.setValue("ns=2;i=9", data.get("uniqueid"))

    opcuaServer.setValue("ns=2;i=12", data.get("state").get("alert"))
    opcuaServer.setValue("ns=2;i=13", data.get("state").get("bri"))
    opcuaServer.setValue("ns=2;i=14", data.get("state").get("colormode"))
    opcuaServer.setValue("ns=2;i=15", data.get("state").get("ct"))
    opcuaServer.setValue("ns=2;i=16", data.get("state").get("effect"))
    opcuaServer.setValue("ns=2;i=17", data.get("state").get("hue"))
    opcuaServer.setValue("ns=2;i=18", data.get("state").get("on"))
    opcuaServer.setValue("ns=2;i=19", data.get("state").get("reachable"))
    opcuaServer.setValue("ns=2;i=20", data.get("state").get("sat"))

@uamethod
def changeLED(parent,status):
    from opcuaServer import opcuaServer
    endpoint = opcuaServer.getValue("ns=2;i=26")
    mqtt = mqttAdapter(endpoint,"publish",False,"","")

    if status is True:
        mqtt.add_param("topic","/esp32/led/on")
        mqtt.add_param("msg","on")
        status = True
    if status is False:
        mqtt.add_param("topic","/esp32/led/off")
        mqtt.add_param("msg","off")
        status = False

    try:
        threading.Thread(target=mqtt.call_endpoint()).start()
        data = mqtt.getAllReturnValue()
    except Exception as e:
        raise ua.UaError("Error calling endpoint")

    if "success" in data:
        opcuaServer.setValue("ns=2;i=36", status)
    else:
        print(data)
        raise ua.UaError("Request not successful")


@uamethod
def dataStreamEsp(parent,flag):
    from opcuaServer import opcuaServer
    if flag is True:
        thread = threading.Thread(target=__loopEsp,args=())
        thread.daemon = True
        thread.start()
        opcuaServer.setValue("ns=2;i=44", True)
    else:
        opcuaServer.setValue("ns=2;i=44", False)


def __loopEsp ():
    from opcuaServer import opcuaServer

    while True:
        endpoint = opcuaServer.getValue("ns=2;i=26")
        mqtt = mqttAdapter(endpoint, "subscribe", False, "", "")
        pinStatus = opcuaServer.getValue("ns=2;i=34")
        LEDstatus = opcuaServer.getValue("ns=2;i=33")
        status = opcuaServer.getValue("ns=2;i=35")

        if status is True:
            mqtt.add_param("activate",True)
            if pinStatus is True:
                mqtt.add_param("pin",True)
                mqtt.add_param("topics",'("/esp32/touch/#",0)"')
            else:
                mqtt.add_param("pin",False)

            if status is True:
                mqtt.add_param("LED",True)
                if pinStatus is True:
                    mqtt.add_param ("topics" ,"/esp32/touch/#,/esp32/led/status")
                else:
                    mqtt.add_param("topics","/esp32/led/status")
        else:
            mqtt.add_param("activate",False)
            status = False

        try:
            threading.Thread(target=mqtt.call_endpoint()).start()
            data = mqtt.getAllReturnValue()
        except Exception as e:
            raise ua.UaError("Error calling endpoint")

        if "topic" in data:
            opcuaServer.setValue("ns=2;i=37",data.get("msg_received"))
            print(data)
        else:
            print(data)
            raise ua.UaError("Request not successful")
        run = opcuaServer.getValue("ns=2;i=44")
        if run is False:
            break


@uamethod
def interactESPwthLight(parent, flag):
    from opcuaServer import opcuaServer
    if flag is True:
        thread = threading.Thread(target=__loop_checkPIN, args=())
        thread.daemon = True
        thread.start()
        opcuaServer.setValue("ns=2;i=43", True)
    else:
        opcuaServer.setValue("ns=2;i=43", False)


def __loop_checkPIN():
    from opcuaServer import opcuaServer

    while True:
        value = opcuaServer.getValue("ns=2;i=37") # change Pin NodeID !!!

        if value < 90:
            try:
                __changeColor("red")
            except Exception as e:
                print("Interaction not possible: " + str(e))
                time.sleep(30)
        if value > 100:
            try:
                __changeColor("green")
            except Exception as e:
                print("Interaction not possible: " + str(e))
                time.sleep(30)
        time.sleep(1)

        run = opcuaServer.getValue("ns=2;i=43")
        if run is False:
            break


def __changeColor(color):
    from opcuaServer import opcuaServer

    if opcuaServer.getValue("ns=2;i=18") is True:
        endpoint = opcuaServer.getValue("ns=2;i=2")
        endpoint = endpoint + "/state"
        rest = restAdapter(endpoint, "put", False, "", "")

        color = color.lower()
        if color == "red":
            rest.add_param("hue", 0)

        elif color == "blue":
            rest.add_param("hue", 44670)

        elif color == "green":
            rest.add_param("hue", 21760)

        elif color == "yellow":
            rest.add_param("hue", 10880)
        else:
            raise ua.UaError("Color is unknown !")
    else:
        raise ua.UaError("Not connected")

    try:
        threading.Thread(target=rest.call_endpoint()).start()
        data = rest.get_returnObj()
        data = max(data)
    except Exception as e:
        raise ua.UaError("Error calling endpoint")

    if "success" in data:
        opcuaServer.setValue("ns=2;i=17", data.get("success").get("/lights/1/state/hue"))
    else:
        raise ua.UaError("Request not successful")