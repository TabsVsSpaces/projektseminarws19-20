import logging
import os
import sys
import time

from opcuaServer import opcuaServer

# NUMBER_ARGUMENTS = 2

if __name__ == '__main__':

    logging.basicConfig(level=logging.WARN)
    print("")

    # if len(sys.argv) != NUMBER_ARGUMENTS:
    #     print('Wrong number of arguments!')
    #     print('Usage: python main.py XML-file')
    # else:
    #    xmlFile = sys.argv[1]

    #    if os.path.exists(xmlFile) and os.path.isfile(xmlFile):
    try:
        # check_serviceMQTT_running
        print("------------------------------------")
        print("Check MQTT Broker...")
        print("------------------------------------")

        mqttService = os.system('systemctl is-active --quiet mosquitto.service')

        if mqttService == 0:
            print('mosquitto.service detected')
        else:
            
            print('No MQTT-Broker service detected!')
            print('Therefore MQTT is not supported!')

        print("")

        # start opcua server
        print("------------------------------------")
        print("Start OPC-UA Server...")
        print("------------------------------------")

        opcuaServer.start()
        # opcuaServer.start(xmlFile)

        print('OPC-UA Server is running...')
        print("")

        # connect to assets
        # opcuaServer.conToAsset()

        while True:
            time.sleep(10)
            print('[' + time.ctime() + '] running...')

    except Exception as e:
        print('ERROR: Could not init server or broker')
        print(e)
    finally:
        opcuaServer.stop()
        print("")
        print("------------------------------------")
        print('Stopped')
        print("------------------------------------")
        sys.exit(1)
else:
    print('File not found')
