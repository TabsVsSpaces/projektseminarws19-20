#!python3
# http://www.steves-internet-guide.com/into-mqtt-python-client/
# http://www.steves-internet-guide.com/client-connections-python-mqtt/

import paho.mqtt.client as mqtt
import time
import logging
from opcua import Server, ua


class mqttMethods:
    # initialisiere Client instanz
    msg = {}
    connectRet = 0
    pubFlag = 1
    msgFlag = 1

    def Initialise_clients(self):
        # callback assignment
        client = mqtt.Client()  # don't use clean session
        client.on_connect = self.on_connect  # attach function to callback
        client.on_message = self.on_message  # attach function to callback
        client.on_subscribe = self.wait_for
        client.on_disconnect = self.on_disconnect
        client.on_publish = self.on_publish
        # flags set
        client.topic_ack = []
        client.run_flag = False
        client.running_loop = False
        client.subscribe_flag = False
        client.bad_connection_flag = False
        client.connected_flag = False
        client.disconnect_flag = False
        return client

    def on_publish(self,client, userdata, result):  # create function for callback
        print("data published \n")
        if result[1] == 0:
            self.msg["successpub"] = True
        else:
            self.msg["failed"] = True
        pass
    def on_connect(self,client, userdata, flags, rc, ):
        if rc == 0:
            client.connected_flag = True  # set flag
            print("connected OK Returned code=", rc)

        else:
            print("Bad connection Returned code=", rc)
            client.bad_connection_flag = True

    def on_disconnect(self, client, userdata, rc=0, ):
        if rc != 0:
            print("Unexpected disconnection.")
            self.msg["return"] = "unexpected disconnect"
        client.loop_stop()
        self.msg["return"] = "disconnect"


    @staticmethod
    def ConnectToBroker(client, host_name,):
        try:
            client.connect(host_name, 1883)  # connect to broker
        except:
            print("connection failed")
            exit(1)

        if client.bad_connection_flag:
            client.loop_stop()  # Stop loop
            client.disconnect()

    def AuthConnectBroker(self, client, username, password, host_name):
        client.username_pw_set(username=username, password=password)
        self.ConnectToBroker(host_name, client)

    # callback functions prints received messages
    def on_message(client, userdata, flags, message):
        from opcuaServer import opcuaServer
        global msgFlag
        msgFlag = 0
        client.subscribe_flag = True
        data = True
        print("msg_received: " + str(message.payload.decode("utf-8")))
        dict = {
            "msg_received": int(message.payload),
            "topic": message.topic,
            "qos": message.qos,
            "retain_flag": message.retain}
        if "pin_1" in dict.get("topic"):
           ret = opcuaServer.setValue("ns=2;i=36", int(message.payload))
           print (ret)
        elif  "pin_2" in dict.get("topic"):
            print("writing pin1")
            ret2 = opcuaServer.setValue("ns=2;i=37",int(message.payload))
            print(ret2)
        elif "status" in message.topic:
            if int(message.payload)>0:
                opcuaServer.setValue("ns=2;i=33",True)
            else:
                opcuaServer.setValue("ns=2;i=33",False)
        global msg
        msg = dict.copy(data)


    """
    topic : String/bytes. topic that the message was published on.
    payload : String/bytes the message payload.
    qos : Integer. The message Quality of Service 0, 1 or 2.
    retain : Boolean. If true, the message is a retained message and not fresh.
    mid : Integer. The message id.
"""

    @staticmethod
    def wait_for(client, msgType, period=0.25):
        if msgType == "SUBACK":
            if client.on_subscribe:
                while not client.suback_flag:
                    print("waiting suback")
                    time.sleep(period)

    def SubscribeTopic(self, client, paramsDict):
        if paramsDict.get("activate") is True:
            topics = paramsDict.get("topics")
            if topics.find(',')!=1:
                print(topics)
                topic1,topic2 = topics.split(',')
                topics = [(topic1,0),(topic2,0)]
            else:
                topics = [(topics,0)]
            sub = self.__subscribeTopic(client, topics)
        else:
            client.disconnect()

        time.sleep(5)
        return self.msg

    def __subscribeTopic(self,client,topics):
        ret = client.subscribe(topics)
        client.loop_forever()
        print(ret)

    def publishTopic(self, client, __dict):
        from opcuaServer import opcuaServer
        active = opcuaServer.getValue("ns=2;i=35")
        msg = __dict.get("msg")
        topic = __dict.get("topic")
        client.publish(__dict.get("topic"),msg)
        while self.pubFlag == 1:
            time.sleep(1)  # wait
            counter = +1
            if (counter > 15):
                self.pubFlag == 0
        if active is False:
            client.disconnect()
            return self.msg


    def disconnect(self, client):
        client.disconnect()
