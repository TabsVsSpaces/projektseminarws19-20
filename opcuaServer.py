from opcua import Server, ua
import customMethods

PROTOCOL = "opc.tcp"
IPv4 = "0.0.0.0"
PORT = "4840"
SERVER_NAME = "OpcUa Test Server"
ENDPOINT = "/server/"


# Singleton
class opcuaServer:
    __instance = None

    @staticmethod
    def __getInstance():
        if opcuaServer.__instance is None:
            opcuaServer.__instance = Server()
        return opcuaServer.__instance

    @staticmethod
    def start():
        if opcuaServer.__instance is None:
            server = opcuaServer.__getInstance()
            server.set_server_name(SERVER_NAME)
            server.set_endpoint(PROTOCOL + "://" + IPv4 + ":" + PORT + ENDPOINT)
            # server.import_xml(xmlFile)

            uri = "http://org.htwdd.org/ProjSem"
            idx = server.register_namespace(uri)

            objects = server.get_objects_node()

            # Anfang Lichtstreifen
            lichtObj = objects.add_object(idx, "Lichtstreifen")
            lichtObj.add_property(idx, "Endpoint", "http://192.168.8.1/api/04A10B5FD5/lights/1")
            lichtObj.add_property(idx, "etag", "")
            lichtObj.add_property(idx, "manufacturer", "")
            lichtObj.add_property(idx, "modelid", "")
            lichtObj.add_property(idx, "name", "")
            lichtObj.add_property(idx, "swversion", "")
            lichtObj.add_property(idx, "type", "")
            lichtObj.add_property(idx, "uniqueid", "")
            lichtObj.add_method(idx, "getAll", customMethods.getFullState, [], [])

            state = lichtObj.add_folder(idx, "state")
            alert = state.add_variable(idx, "alert", "")
            alert.set_writable()
            bri = state.add_variable(idx, "bri", 0, ua.VariantType.Int16)
            bri.set_writable()
            colormode = state.add_variable(idx, "colormode", "")
            colormode.set_writable()
            ct = state.add_variable(idx, "ct", 0, ua.VariantType.Int16)
            ct.set_writable()
            effect = state.add_variable(idx, "effect", "")
            effect.set_writable()
            hue = state.add_variable(idx, "hue", 0, ua.VariantType.Int32)
            hue.set_writable()
            on = state.add_variable(idx, "on", False, ua.VariantType.Boolean)
            on.set_writable()
            reachable = state.add_variable(idx, "reachable", False, ua.VariantType.Boolean)
            reachable.set_writable()
            sat = state.add_variable(idx, "sat", 0, ua.VariantType.Int16)
            sat.set_writable()

            state.add_method(idx, "shift", customMethods.shift, [], [])
            state.add_method(idx, "changeColor", customMethods.changeColor, [ua.VariantType.String], [])
            # Ende Lichtstreifen

            # Anfang ESP32
            espObj = objects.add_object(idx,"ESP32Modul")
            espObj.add_property(idx, "ESP32","")
            espObj.add_property(idx, "endpoint", "broker.hivemq.com")
            espObj.add_property(idx, "manufacturer", "joy-it")
            espObj.add_property(idx, "model", "SBC-NodeMCU-ESP32")
            espObj.add_property(idx, "type", "ESP32")
            espObj.add_property(idx,"uniqueid","")
            espObj.add_method(idx, "getAll", customMethods.getFullState, [], [])

            funk = espObj.add_folder(idx,"Functions")
            led = funk.add_variable(idx, "LED_On", False,ua.VariantType.Boolean)
            led.set_writable()
            touch = funk.add_variable(idx, "PinTouchActive", True)
            touch.set_writable()
            status = funk.add_variable(idx, "Status", True, ua.VariantType.Boolean)
            status.set_writable()
            pin1 = funk.add_variable(idx,"pin1",0,ua.VariantType.UInt16)
            pin1.set_read_only()
            pin2 = funk.add_variable(idx,"pin2",0,ua.VariantType.UInt16)
            pin2.set_read_only()


            funk.add_method(idx, "changeLED", customMethods.changeLED, [ua.VariantType.Boolean], [])
            data_stream_esp = funk.add_method(idx, "dataStreamEsp", customMethods.dataStreamEsp, [ua.VariantType.Boolean], [])
            # Ende ESP32

            interact = objects.add_method(idx, "interact", customMethods.interactESPwthLight, [ua.VariantType.Boolean], [])
            run = interact.add_property(idx, "interact", False, ua.VariantType.Boolean)
            runesp = data_stream_esp.add_property(idx,"running",False, ua.VariantType.Boolean)

            server.start()
        else:
            raise Exception("A OPC-UA Server is already running!")

    @staticmethod
    def stop():
        if opcuaServer.__instance is not None:
            server = opcuaServer.__getInstance()
            server.stop()

    @staticmethod
    def getValue(nodeID):
        if opcuaServer.__instance is not None:
            server = opcuaServer.__getInstance()
            node = server.get_node(nodeID)
            value = node.get_value()
            return value
        else:
            raise Exception("OPC-UA Server is not running!")

    @staticmethod
    def setValue(nodeID, value):
        if opcuaServer.__instance is not None:
            server = opcuaServer.__getInstance()
            node = server.get_node(nodeID)
            node.set_value(value)
        else:
            raise Exception("OPC-UA Server is not running!")
