# https://www.w3schools.com/python/module_requests.asp
# https://dresden-elektronik.github.io/deconz-rest-doc/getting_started/

import requests
from requests.auth import HTTPDigestAuth
import json

TIMEOUT = 10


class restMethods:

    @staticmethod
    def get_request_noCred(url, method, params):
        request = getattr(requests, method)
        myRequest = request(url, params=params, timeout=TIMEOUT)

        if myRequest.ok:
            jsonData = json.loads(myRequest.content)
            return jsonData
        else:
            myRequest.raise_for_status()

    @staticmethod
    def get_request_wthCred(url, method, params, username, password):
        request = getattr(requests, method)
        myRequest = request(url, params=params, timeout=TIMEOUT, auth=HTTPDigestAuth(username, password), verify=True)

        if myRequest.ok:
            jsonData = json.loads(myRequest.content)
            return jsonData
        else:
            myRequest.raise_for_status()

    @staticmethod
    def put_request_noCred(url, method, params):
        request = getattr(requests, method)
        myRequest = request(url, data=params, timeout=TIMEOUT)

        if myRequest.ok:
            jsonData = json.loads(myRequest.content)
            return jsonData
        else:
            myRequest.raise_for_status()

    @staticmethod
    def put_request_wthCred(url, method, params, username, password):
        request = getattr(requests, method)
        myRequest = request(url, data=params, timeout=TIMEOUT, auth=HTTPDigestAuth(username, password), verify=True)

        if myRequest.ok:
            jsonData = json.loads(myRequest.content)
            return jsonData
        else:
            myRequest.raise_for_status()

    @staticmethod
    def post_request_noCred(url, method, params):
        request = getattr(requests, method)
        myRequest = request(url, data=params, timeout=TIMEOUT)

        if myRequest.ok:
            jsonData = json.loads(myRequest.content)
            return jsonData
        else:
            myRequest.raise_for_status()

    @staticmethod
    def post_request_wthCred(url, method, params, username, password):
        request = getattr(requests, method)
        myRequest = request(url, data=params, timeout=TIMEOUT, auth=HTTPDigestAuth(username, password), verify=True)

        if myRequest.ok:
            jsonData = json.loads(myRequest.content)
            return jsonData
        else:
            myRequest.raise_for_status()
